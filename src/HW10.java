import java.util.Random;
import java.util.Scanner;
public class HW10 {
   public static void main(String[] args) {

        //1. Calculate the factorial of the number n.

        System.out.println("Enter a non-negative integer: ");
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        int i;
        int factorial = 1;
        for (i = 1; i <= n; i++) {
            factorial *= i;
        }
        System.out.println(n + " factorial=: " + factorial);

        //2. Calculate the sum of the digits of a given number.

        System.out.println("Enter the number");
        Scanner input = new Scanner(System.in);
        int number = input.nextInt();
        int n;
        int sum = 0;
        while (number > 0) {
            n = number % 10;
            sum += n;
            number = number / 10;
        }
        System.out.println("The sum of the digits of the number is: " + sum);

        //3. Find a number that is a mirror image of the sequence of digits of a given number,
        // for example, given the number 123, output 321.

        System.out.print("Enter the number: ");
        Scanner input = new Scanner(System.in);
        int number = input.nextInt();
        int mirrorNumber = 0;
        while (number != 0) {
            int n = number % 10;
            mirrorNumber = (mirrorNumber * 10) + n;
            number /= 10;
        }
        System.out.print(" Mirror number is: " + mirrorNumber);

        //4. Find the minimum element of an array.

        int number[] = {2, 4, 6, 8};
        int minElement = 0;
        for (int i = 0; i < number.length; i++) {
            minElement = number[0];
        }
        System.out.println("The minimum element of an array " + minElement);

        //5. Find the index of the minimum element of the array.

        int number[] = {2, 4, 1, 8};
        int minIndex = 0;
        for (int i = 0; i < number.length; i++) {
            if (number[i] < number[minIndex]) {
                minIndex = i;
            }
        }
        System.out.println("The index of the minimum element " + minIndex);

        //6. Calculate the sum of array elements with odd indexes.

        int number[] = {3, 5, 6, 8, 15, 7, 9};
        int sum = 0;
        for (int i = 0; i < number.length; i++) {
            if (i % 2 == 0) {
                sum += number[i];

            }
        }
        System.out.println("Sum of array elements with odd indexes = " + sum);

        //7.Make an array reverse (an array in the opposite direction).

        int number[] = {1, 2, 3, 4, 5};
        System.out.println("Original array: ");
        for (int i = 0; i < number.length; i++) {
            System.out.print(number[i] + " ");
        }
        System.out.println();
        System.out.println("Array in reverse order: ");
        for (int i = number.length - 1; i >= 0; i--) {
            System.out.print(number[i] + " ");
        }

    //8. Count the number of odd elements of the array.

        int number[] = {3, 5, 6, 8, 15, 7, 9, 1};
        int count = 0;
        for (int i = 0; i < number.length; i++) {
            if (number[i] % 2 != 0) {
                count += 1;
            }
        }
        System.out.println("Count the number of odd elements = " + count);

    //9. Swap the first and second half of the array, for example, for the array 1 2 3 4, the result is 3 4 1 2.

        int number[] = {1, 2, 3, 4, 5, 6, 7, 8};
        System.out.println("orijinal array: ");
        int i;
        int count = 0;
        for (i = 0; i < number.length; i++) {
            System.out.print(number[i] + " ");
            count++;
        }
        System.out.println();
        int half = count / 2;
        while (half < number.length) {
            System.out.print(number[half] + " ");
            half++;
        }
        int min = 0;
        while (min < (count / 2)) {
            System.out.print(number[min] + " ");
            min++;
        }

      //1. Write a program that prints all even numbers from 1 to 20 using a while loop.

       int i=1;
       while (i <= 20) {
           if (i % 2 ==0){
           System.out.println(i); }
           i++;
       }

     //2. Write a program that calculates the sum of all numbers from 1 to 10 using a for loop.

      int sum=0;
       int i;
       for (i=1; i<=10; i++){
           sum +=i;
       }
       System.out.println("sum of all numbers from 1 to 10 : " +sum);

    //3. Write a program that prompts the user to enter a number and keeps asking for input until the user enters a negative number.
        //Then, calculate and print the sum of all positive numbers entered.

       Scanner input = new Scanner(System.in);
       int number;
       int sum=0;

       do {
           System.out.println("Enter the pozitive number: ");
           number= input.nextInt();

           if (number >= 0){
               sum += number;
           }
       } while ( number >= 0 );

       System.out.println("Sum of all positive numbers: " +sum);

    //4. Write a program that prints the multiplication table of a given number using a for loop.
        //The program should take the number as input from the user.

       System.out.print("Enter the pozitive number: ");
       Scanner input = new Scanner(System.in);
       int number = input.nextInt();
       for (int i=1; i<=10; i++){
            int sum= number *i;
           System.out.println( number+ "*" +i+ "=" +sum);
       }

   //5. Write a program that generates and prints a random number between 1 and 100.
       // The program should keep generating numbers until it generates a number that is divisible by 7.

       Random random = new Random();
       int number;

       do {
           number = random.nextInt(100)+1;
           System.out.println("Generated number: " + number);

       } while (number % 7 != 0);

       System.out.println("Generated number divisible by 7: " + number);


    }
}
